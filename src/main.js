const data = [
  {
    name: "Regina",
    base: "tomate",
    price_small: 6.5,
    price_large: 9.95,
    image:
      "https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300",
  },
  {
    name: "Napolitaine",
    base: "tomate",
    price_small: 6.5,
    price_large: 8.95,
    image:
      "https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300",
  },
  {
    name: "Spicy",
    base: "crème",
    price_small: 5.5,
    price_large: 8,
    image:
      "https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300",
  },
];

import {Component} from './components/Component.js';
import {Img} from'./components/Img.js';

const title = new Component("h1", null, "La carte");
document.querySelector(".pageTitle").innerHTML = title.render();

const img = new Img(
    "https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300",
);

document.querySelector(".pageContent").innerHTML = img.render();

// // Attention : data.sort() modifie le tableau original
// render( data.sort(sortByName) );
// render( data.sort(sortByPrice) );

// // G.2. Système de filtre
// render( data.filter( pizza => pizza.base == 'tomate' ))
// render( data.filter( pizza => pizza.price_small < 6 ))
// // deux possibilités pour les pizzas avec deux fois la lettre "i"
// render( data.filter( pizza => pizza.name.split('i').length == 3 ))
// render( data.filter( pizza => pizza.name.match(/i/g).length == 2 ))

// // G.3. Destructuring
// render( data.filter( ({base}) => base == 'tomate' ))
// render( data.filter( ({price_small}) => price_small < 6 ))
// render( data.filter( ({name}) => name.match(/i/g).length === 2 ));
