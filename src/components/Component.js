export class Component {
  tagName;
  children;
  attribute;

  constructor(tagName, attribute, children) {
    this.tagName = tagName;
    this.children = children;
    this.attribute = attribute;
  }

  render() {
    if (this.attribute != null) {
      return this.renderAttribute();
    }
    return `<${this.tagName}>${this.children}</${this.tagName}>`;
  }

  renderAttribute() {
    return `<${this.tagName} ${this.attribute.name}="${this.attribute.value}"/>`;
  }
}
