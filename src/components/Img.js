import { Component } from "./Component.js";

export class Img extends Component {
  constructor(src) {
    super("img", { name: "src", value: src }, null);
  }
}
